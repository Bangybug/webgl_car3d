var cars = {

	"audi_s3":  { model_index: "m_01/a_01/c_01/" },
	"aston_martin_vintage_v8": { model_index: "m_01/am_01/c_01/" },
	"chevrolet_camaro": { model_index: "m_01/ch_01/c_01/" },
	"ferrari_california": { model_index:"m_01/f_01/c_01/" },
	"lancer_evo": { model_index: "m_01/mi_01/c_01/"},
	"mazda3": { model_index: "m_01/ma_01/c_01/" },
	"nissan350z": { model_index: "m_01/n_01/c_01/"},
	"slr500": { model_index: "m_01/me_01/c_01/"},
    
    "audi_a1": 
    {
        "body": [
            {
                label: "body",
                mesh: "m_01/a1_sport/body.js",
                texture: "m_01/a1_sport/Audi-A11.jpg",
                color: 0xffffff,
                emissive: 0xffffff,
                shininess: 60,
                canChangeColor:true,
                canChangeTexture:true,
                decalTarget: true
            },
            {
                label: "mirrors",
                mesh: "m_01/a1_sport/mirrors.js",
                color: 0xff0000,
                emissive: 0x111111,
                shininess: 60,
                canChangeColor:true
            },
            {
                label: "top",
                mesh: "m_01/a1_sport/top.js",
                color: 0xffffff,
                canChangeColor:true,
                decalTarget: true
            }
        ],
        "chrome": [
            {
                color: 0xffffff,
                emissive: 0x111111,
                shininess: 60,
                mesh: "m_01/a1_sport/chrome.js"
            }
        ],
        "glass": [
            {
                mesh: "m_01/a1_sport/glass.js",
                color: 0xffffff,
                emissive: 0xffffff,
                shininess: 60
            }
        ],
        "plastic": [
            {
                color: 0x1C1C1C,
                mesh: "m_01/a1_sport/plastic.js"
            }
        ],
        "scale" : [60,60,60]
    }

}